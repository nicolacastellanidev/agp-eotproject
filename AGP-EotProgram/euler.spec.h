#pragma once

namespace EulerSpec {
	char shouldCreateWithDefaultConstructor();
	char shouldCreateWithConstructor();
	char shouldConvertEulerToMatrix();
	char shouldConvertEulerToAngleAxis();
	char shouldCheckEqualityCorrectly();
	char shouldApplyWithVector3Correctly();
	char shouldApplyXRotationCorrectly();
	char shouldApplyYRotationCorrectly();
	char shouldApplyZRotationCorrectly();
	char shouldConvertToMatrixAndBack();
}