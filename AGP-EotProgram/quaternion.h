#pragma once

#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"
#include <type_traits>

/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Matrix3;
class AxisAngle;
class Euler;

typedef double Scalar;

class Quaternion{
public:

    /* fields */
    // DONE Q-Fields: which fields to store? (also add a constuctor taking these fields).


    Vector3 v; // imaginary part
    Scalar d;       // real part

    // DONE Q-Constr
    Quaternion(Scalar _a, Scalar _b, Scalar _c, Scalar _d) 
        : v(_a, _b, _c), d(_d) {}

	Quaternion(const Vector3& _v, Scalar _d)
		: v(_v), d(_d) {}

	Quaternion(const Vector3& _v)
		: v(_v), d(0) {}

	Quaternion(Scalar _d)
		: v(), d(_d) {}

    // DONE Q-Ide: this constructor construct the identity rotation
    Quaternion() 
        : v(0, 0, 0), d(0) {}

    // TODO Q-FromPoint
    // returns a quaternion encoding a point
    Quaternion( const Point3& p )
        : v(p), d(0) {}

    Vector3 apply( Vector3  v) const {
        // TODO Q-App: how to apply a rotation of this type?
        return Vector3();
    }

    // Rotations can be applied to versors or vectors just as well
    Versor3 apply( Versor3 dir ) const {
        return apply( dir.asVector() ).asVersor();
    }

    Point3 apply( Point3 p ) const {
        return apply( p.asVector() ).asPoint();
    }

    bool isEqual(const Quaternion& other) {
        return v.isEqual(other.v)
            && d == other.d;
    }

    // syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
    Versor3 operator() (Versor3 p) { return apply(p); }
    Point3  operator() (Point3  p) { return apply(p); }
    Vector3 operator() (Vector3 p) { return apply(p); }

    Versor3 axisX() const {
        // DONE Q-Ax
        return normalize(Vector3(v.x, 0, 0));
    }
	Versor3 axisY() const {
		// DONE Q-Ay
		return normalize(Vector3(0, v.y, 0));
	}
	Versor3 axisZ() const {
		// DONE Q-Az
		return normalize(Vector3(0, 0, v.z));
	}

    // conjugate
    Quaternion operator * (Quaternion r) const {
        return Quaternion();
    }

    Quaternion inverse() const{
        // TODO Q-Inv a
        return Quaternion();
    }

    void invert() const{
        // TODO Q-Inv b
    }

    // specific methods for quaternions...
    Quaternion conjugated() const{
        // TODO Q-Conj a
        return Quaternion();
    }

    void conjugate(){
        // TODO Q-Conj b
    }

    // returns a rotation to look toward target, if you are in eye, and the up-vector is up
    static Quaternion lookAt( Point3 eye, Point3 target, Versor3 up = Versor3::up() ){
        // TODO Q-LookAt
        return Quaternion();
    }

    // returns a rotation
    static Quaternion toFrom( Versor3 to, Versor3 from ){
        // TODO Q-ToFrom
        return Quaternion();
    }

    static Quaternion toFrom( Vector3 to, Vector3 from ){
        return toFrom( normalize(to) , normalize(from) );
    }

    // conversions to this representation
    static Quaternion from( Matrix3 m );   // TODO M2Q
    static Quaternion from( Euler e );     // TODO E2Q
    static Quaternion from( AxisAngle e ); // TODO A2Q

	bool isUnit() const {
        Scalar normV = norm(v);
        Scalar result = normV * normV + d * d;
        return result >= 1 - EPSILON && result <= 1 + EPSILON;
	}

    // does this quaternion encode a rotation?
    bool isRot() const{
        // DONE Q-isR
        return isUnit() && !isPoint();
    }

    // does this quaternion encode a point?
    bool isPoint() const{
        // Done Q-isP
        return d == 0 && !isUnit();
    }

    void printf() const {} // TODO Print
};


// interpolation or roations
inline Quaternion lerp( const Quaternion& a,const Quaternion& b, Scalar t){
    // TODO Q-Lerp: how to interpolate quaternions
    // hints: shortest path! Also, consdider them are 4D unit vectors.
    return Quaternion();
}



