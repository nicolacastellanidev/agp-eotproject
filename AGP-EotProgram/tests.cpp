#include <iostream>
#include <math.h>
#include "euler.h"
#include "transform.h"
#include "Print.h"
#include "shared.h"
#include "matrix3.h"
#include <iostream>

char unitTestInverse() {
	Transform t;
	t.translation = Vector3(2, 0, 0);
	t.scale = 3;

	Point3 p0, q, p1;

	q = t.apply(p0);

	Transform ti = t.inverse();
	p1 = ti.apply(q); // VOGLIO RIOTTENERE p

	char result = check(p0.isEqual(p1), "INVERSE");
	return result;
}


char unitTestCumulate() {
	Transform t1, t2;
	t1.translation = Vector3(2, 0, 0);
	t1.scale = 4;
	t1.rotation = Euler::from(Matrix3::rotationX(90));

	t2.translation = Vector3(4, -3, 1);
	t2.scale = 0.4;
	t2.rotation = Euler::from(Matrix3::rotationZ(90));

	Point3 p(3, 1, 3);
	Point3 q0 = t2.apply(t1.apply(p));
	Transform product = t2 * t1;
	Point3 q1 = product.apply(p);

	char result = check(q0.isEqual(q1), "CUMULATE");
	return result;
}