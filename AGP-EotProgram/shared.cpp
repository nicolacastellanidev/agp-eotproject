#include "shared.h"
#include <iostream>
#include "Print.h"
char check(bool condition, std::string descr) {
	Nica::PrintSpaceBetween({ ((condition) ? "[PASSED]" : "[FAILED]") }, { descr }, condition ? FOREGROUND_GREEN : FOREGROUND_RED);
	return condition;
}