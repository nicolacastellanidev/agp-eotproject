#include "quaternions.spec.h"
#include "quaternion.h"
#include "shared.h"
#include "versor3.h"
#include "Print.h"

char QuaternionSpecs::shouldCreateCorrectlyWithDefaultConstructor()
{
	Quaternion q = Quaternion();
	char result = check(q.isEqual(Quaternion(0,0,0,0)), "shouldCreateCorrectlyWithDefaultConstructor");
	return result;
}

char QuaternionSpecs::shouldCreateCorrectlyWithConstructorWithParams()
{
	Quaternion q = Quaternion(0, 0, 0, 0);
	char result = check(q.isEqual(Quaternion()), "shouldCreateCorrectlyWithConstructorWithParams");
	return result;
}

char QuaternionSpecs::shouldCreateCorrectlyWithPoint3()
{
	Quaternion q = Quaternion(Point3(1, 2, 3));
	Quaternion expected = Quaternion(1, 2, 3, 0);
	char result = check(q.isEqual(expected), "shouldCreateCorrectlyWithPoint3");
	return result;
}

char QuaternionSpecs::shouldReturnCorrectlyAxisX()
{
	Quaternion q = Quaternion(Point3(1, 2, 3));
	Versor3 expected = Vector3(1, 0, 0).asVersor();
	char result = check(q.axisX().isEqual(expected), "shouldReturnCorrectlyAxisX");
	return result;
}

char QuaternionSpecs::shouldReturnCorrectlyAxisY()
{
	Quaternion q = Quaternion(Point3(1, 2, 3));
	Versor3 expected = Vector3(0, 1, 0).asVersor();
	char result = check(q.axisY().isEqual(expected), "shouldReturnCorrectlyAxisY");
	return result;
}

char QuaternionSpecs::shouldReturnCorrectlyAxisZ()
{
	Quaternion q = Quaternion(Point3(1, 2, 3));
	Versor3 expected = Vector3(0, 0, 1).asVersor();
	char result = check(q.axisZ().isEqual(expected), "shouldReturnCorrectlyAxisZ");
	return result;
}

char QuaternionSpecs::shouldReturnCorrectlyIsRot()
{
	Quaternion q = Quaternion(0.5, 0.5, 0.5, 0.5);
	char result = check(q.isRot(), "shouldReturnCorrectlyIsRot");
	return result;
}

char QuaternionSpecs::shouldReturnCorrectlyIsPoint()
{
	Quaternion q = Quaternion(0.5, 0.5, 0.5, 0);
	char result = check(q.isPoint(), "shouldReturnCorrectlyIsPoint");
	return result;
}
