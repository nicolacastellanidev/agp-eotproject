#pragma once


// problems.h:
// a small set of matematical problems

#include "vector3.h"
#include "point3.h"
#include "versor3.h"
#include "Math.h"
#include <math.h>

// POD -- Plain Old Data
struct Ray{
    Point3 p;
    Vector3 v;
};

struct Sphere{
    Point3 c;
    Scalar r;
};

struct Plane {
    // DONE-plane: fill
    Point3 center;
    Versor3 normal;
    Vector3 scale;
};

const Point3 NO_INTERSECTION(666,666,666);

Point3 intersect(const Ray &r, const Sphere &s){
    Scalar a = dot( r.v , r.v );
    Scalar b = 2*dot( r.p-s.c , r.v );
    Scalar c = dot(r.p-s.c,r.p-s.c) - s.r * s.r;

    // euqation is: a*k^2 + b*k + c == 0

    Scalar delta = b*b-4*a*c;
    if (delta < 0) return NO_INTERSECTION;

    Scalar k = (-b -sqrt(delta)) / (2*a);
    if (k<0) return NO_INTERSECTION;
    return r.p + k * r.v;
}

// normal of a plane, given three points
Versor3 planeNormal(const Versor3& P, const Versor3& Q, const Versor3& R) {
	// TODO: P-planeNorm
	Vector3 dir = cross(Q - P, R - P);
	return normalize(dir);
}

Point3 intersect(const Ray &r, const Plane &p){
    // Done: P-inter-plane

	// use 3 arbitrary points, personally I use the center, the extreme horizontal and vertical point
    Vector3 p2 = p.center.asVector() + Vector3(p.scale.x, 0, 0);
    Vector3 p3 = p.center.asVector() + Vector3(0, 0, p.scale.z);
	Versor3 pNormal = planeNormal(
		normalize(p.center.asVector()),
        normalize(p2),
        normalize(p3)
	);

    Scalar denominator = dot(pNormal, r.v);

	// EPSILON is an arbitrary epsilon value. We just want
	// to avoid working with intersections that are almost
	// orthogonal.
	if (abs(denominator) > EPSILON) {

		// Get a point along the ray
        // calc the distance
		Scalar pointToPlaneDistance = p.center.getDistanceTo(r.p);

        // and the difference
		Vector3 diff = p.center - r.p;
		Scalar t = dot(diff, pNormal.asVector());

		if (t > EPSILON) {
            Point3 pointOnRay = r.p + (r.v * pointToPlaneDistance);
			return pointOnRay;
		}
	}

	return NO_INTERSECTION;
}

// does a eye in a given position sees the target, given the angle cone and maxDist?
bool isSeen( const Point3 &eye, const Point3 &target, const Versor3& viewDir, Scalar angleDeg , Scalar maxDist){
    // TODO: isSeen
    Scalar dist = eye.getDistanceTo(target);
    // first, check if distance between points is not greater than maxDist
    if (dist > maxDist) return false;
	// Find cosine of angle between view direction and the vector 
	// connecting q to p
    Versor3 connecting = normalize(target - eye);
    Scalar angle = viewDir.getAngleWith(connecting);
    Scalar cosAngle = Nica::cos(angle);
    Scalar cosView = Nica::cos(angleDeg / 2);
    // Determine if this cosine is > cos 60�/2
    return cosAngle > cosView;
}

// returns the reflected direction of something bouncing in n
Versor3 reflect( const Versor3 &d, const Versor3 &n){
    // TODO: P-reflect
    return Versor3::up();
}

bool areCoplanar(const Versor3 &a, const Versor3 &b, const Versor3 &c){
    // TODO: P-coplanar
    return false;
}

// return a versor as similar as possible to a, but ortogonal to b
Versor3 orthogonalize( const Versor3 &a, const Versor3 &b ){
    // TODO: P-ortogonalize
    return Versor3::up();
}

// a bullet is in position pa and has velocity va
// a target is in position pb and has velocity vb
// returns the position of the target in the moment it is closest to the bullet
Point3 hitPos( const Point3 &pa, const Vector3 &va, const Point3 &pb, const Vector3 &vb ){
    // TODO: P-hitPos
    return Point3();
}



