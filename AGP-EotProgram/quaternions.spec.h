#pragma once

namespace QuaternionSpecs {
	char shouldCreateCorrectlyWithDefaultConstructor();
	char shouldCreateCorrectlyWithConstructorWithParams();
	char shouldCreateCorrectlyWithPoint3();
	char shouldReturnCorrectlyAxisX();
	char shouldReturnCorrectlyAxisY();
	char shouldReturnCorrectlyAxisZ();
	char shouldReturnCorrectlyIsRot();
	char shouldReturnCorrectlyIsPoint();
}