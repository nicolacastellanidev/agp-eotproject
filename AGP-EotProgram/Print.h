#pragma once
#include <iostream>
#include <windows.h>
#include <vector>
/// <summary>
/// Utility classes for print
/// </summary>
namespace Nica {

	typedef std::string String;
	typedef std::vector<String> StringArray;

	template<class U>
	String PrintPair(String prefix, const U value, WORD consoleTextAttr = FOREGROUND_INTENSITY);

	template<>
	String PrintPair(String prefix, const double value, WORD consoleTextAttr);

	/// <summary>
	/// Print a break
	/// </summary>
	void PrintHR();

	/// <summary>
	/// Print a string left and another on the left
	/// </summary>
	void PrintSpaceBetween(StringArray left, StringArray right, WORD consoleTextAttr = FOREGROUND_INTENSITY);

	/// <summary>
	/// Prints a title in console
	/// </summary>
	/// <param name="title">std::string</param>
	void PrintTitle(String title, WORD consoleTextAttr = FOREGROUND_INTENSITY, UINT padding = 5);

	/// <summary>
	/// Prints an empty row
	/// </summary>
	void PrintNewLine();

	/// <summary>
	/// Set console text attribute.
	/// Pass no argument for default attribute.
	/// </summary>
	/// <param name="consoleTextAttr"></param>
	void SetConsoleAttribute(WORD consoleTextAttr = FOREGROUND_INTENSITY);

}