#include "euler.h"
#include "Print.h"
#include "axis_angle.h"
#include "matrix3.h"
#include "Math.h"
#include <cassert>

Euler::Euler(Scalar m00, Scalar m01, Scalar m02, Scalar m10, Scalar m11, Scalar m12, Scalar m20, Scalar m21, Scalar m22)
{
	// first, check if is a rotation matrix
	Matrix3 m = Matrix3(m00, m01, m02, m10, m11, m12, m20, m21, m22);
	assert(m.isRot());

	Euler e = Euler::from(m);
	pitch = e.pitch;
	yaw = e.yaw;
	roll = e.roll;
}

bool Euler::operator==(Euler b) const
{
	return yaw == b.yaw
		&& pitch == b.pitch
		&& roll == b.roll;
}

Vector3 Euler::apply(Vector3 v) const
{
	Matrix3 rotationMatrix = Matrix3::from(Euler(pitch, yaw, roll));
	// multiply rotation matrix to axes
	Vector3 result = rotationMatrix * v;
	return result;
}

Versor3 Euler::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 Euler::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

Versor3 Euler::operator()(Versor3 p)
{
	return apply(p);
}

Point3 Euler::operator()(Point3 p)
{
	return apply(p);
}

Vector3 Euler::operator()(Vector3 p)
{
	return apply(p);
}

Versor3 Euler::axisX() const
{
	// DONE E-Ax a
	return normalize(Vector3(pitch, 0, 0));
}

Versor3 Euler::axisY() const
{
	// DONE E-Ax b
	return normalize(Vector3(0, yaw, 0));
}

Versor3 Euler::axisZ() const
{
	// DONE E-Ax c
	return normalize(Vector3(0, 0, roll));
}

Euler Euler::operator*(Euler b) const
{
	Matrix3 m1 = Matrix3::from(Euler(pitch, yaw, roll));
	Matrix3 m2 = Matrix3::from(Euler(b.pitch, b.yaw, b.roll));
	Matrix3 product = m1 * m2;
	return  Euler::from(m1*m2);
}

Euler Euler::inverse() const
{
	// DONE E-Inv a
	Matrix3 rotationMatrix = Matrix3::from(Euler(pitch, yaw, roll));

	// transpose matrix
	rotationMatrix.inverse();

	// convert to euler
	return Euler::from(rotationMatrix);
}

bool Euler::isEqual(const Euler& b)
{
	return squaredNorm(Vector3(pitch, yaw, roll) - Vector3(b.pitch, b.yaw, b.roll)) < EPSILON2;
}

void Euler::invert()
{
	// DONE E-Inv b
	Euler e = inverse();

	pitch = e.pitch;
	yaw = e.yaw;
	roll = e.roll;
}

Euler Euler::transposed() const {
	// DONE E-Transp a
	// euler transposed matrix is equal to its inverse
	return inverse();
}

void Euler::transpose()
{
	// DONE E-Transp b
	Euler e = inverse();

	pitch = e.pitch;
	yaw = e.yaw;
	roll = e.roll;
}

Euler Euler::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	// Done E-LookAt

	// get look direction
	Vector3 v = (eye - target) / norm(target - eye);
	// get x,y,z vectors
	Vector3 z = v;
	Vector3 uXz = cross(up.asVector(), z);
	Vector3 x = uXz / norm(uXz);
	Vector3 y = cross(z, x);

	Matrix3 rotationMatrix = Matrix3(
		x.x, y.x, z.x,
		x.y, y.y, z.y,
		x.z, y.z, z.z
	);

	return Euler::from(rotationMatrix);
}

Euler Euler::toFrom(Versor3 to, Versor3 from)
{
	// DONE E-ToFrom
	// start with toFrom using axis angle, which is simple than euler tofrom
	AxisAngle angle = AxisAngle::toFrom(to, from);
	// and then convert to euler
	return Euler::from(angle);
}

Euler Euler::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from));
}

Euler Euler::from(Euler e)
{
	// DONE E2M
	return Euler(e.pitch, e.yaw, e.roll);
}

Euler Euler::from(AxisAngle e)
{
	// La scelta migliore sarebbe quella di convertire gli axis in matrici
	// per controllare se c'� singolarit� ai poli e per verificare che il risultato sia
	// una matrice di rotazione.
	// 
	// 
	// Ad ogni modo ho voluto sperimentare questa conversione diretta. Non tiene conto della singolarit�. 

	// Done E2M
	Scalar sinA = Nica::sin(e.angle);
	Scalar cosA = Nica::cos(e.angle);
	Scalar x2 = e.axis.x * e.axis.x;
	Scalar y2 = e.axis.y * e.axis.y;
	Scalar z2 = e.axis.y * e.axis.y;

	// asin(x * y * (1 - cos(angle)) + z * sin(angle))
	Scalar pitch = Nica::asin(e.axis.x * e.axis.y * (1 - cosA) + e.axis.z * sinA);

	// atan2(y * sin(angle)- x * z * (1 - cos(angle)) , 1 - (y2 + z2 ) * (1 - cos(angle)))
	Scalar yaw = Nica::atan2(
		e.axis.y * sinA - e.axis.x * e.axis.z * (1 - cosA),
		1 - (y2 + z2) * (1 - cosA));

	// atan2(x * sin(angle)-y * z * (1 - cos(angle)) , 1 - (x2 + z2) * (1 - cos(angle)))
	Scalar roll = Nica::atan2(
		e.axis.x * sinA - e.axis.y * e.axis.z * (1 - cosA),
		1 - (x2 + z2) * (1 - cosA));

	return Euler(pitch, yaw, roll);
}

Euler Euler::from(Matrix3 m)
{
	// Calculate the sine of the pitch angle
	double sth = -m.row3.x;
	sth = (sth >= 1.0 ? 1.0 : (sth <= -1.0 ? -1.0 : sth)); // Coerce sth to [-1,1]

	// Calculate the required Euler angles
	Scalar roll = atan2(m.row2.x, m.row1.x);
	Scalar yaw = asin(sth);
	Scalar pitch = atan2(m.row3.y, m.row3.z);

	return Euler(
		Nica::fromRadToDegree(pitch),
		Nica::fromRadToDegree(yaw),
		Nica::fromRadToDegree(roll));
}

bool Euler::isRot() const
{
	// Done E-isR
	return Matrix3::from(Euler(pitch, yaw, roll)).isRot();
}

Euler Euler::rotationX(Scalar angleDeg)
{
	// Done E-Rx
	return Euler(angleDeg, 0, 0);
}

Euler Euler::rotationY(Scalar angleDeg)
{
	// Done E-Ry
	return Euler(0, angleDeg, 0);
}

Euler Euler::rotationZ(Scalar angleDeg)
{
	// Done E-Rz
	return Euler(0, 0, angleDeg);
}

void Euler::printf() const
{
	std::cout
		<< Nica::PrintPair("pitch", pitch) << " "
		<< Nica::PrintPair("yaw", yaw) << " "
		<< Nica::PrintPair("roll", roll) << " "
		<< "\n";
} // DONE Print

inline Euler directLerp(const Euler& a, const Euler& b, Scalar t)
{
	// DONE E-directLerp: how to interpolate Eulers

	// convert euler to vector
	Vector3 v1 = Vector3(a.pitch, a.yaw, a.roll);
	Vector3 v2 = Vector3(b.pitch, b.yaw, b.roll);
	Vector3 result = lerp(v1, v2, t);
	// then back from vector
	return Euler(result.x, result.y, result.z);
}

inline Euler lerp(const Euler& a, const Euler& b, Scalar t)
{
	// DONE E-smartLerp: how to interpolate Eulers
	// convert euler t axis
	AxisAngle a_angle = AxisAngle::from(a);
	AxisAngle b_angle = AxisAngle::from(b);

	// then back from axis
	return Euler::from(lerp(a_angle, b_angle, t));
}
