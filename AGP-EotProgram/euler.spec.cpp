#include "euler.spec.h"
#include "euler.h"
#include "Print.h"
#include "shared.h"
#include "matrix3.h"
#include "axis_angle.h"

char EulerSpec::shouldCreateWithDefaultConstructor()
{
	Euler e = Euler();
	char result = check(e.isEqual(Euler(0,0,0)), "shouldCreateWithDefaultConstructor");
	return result;
}

char EulerSpec::shouldCreateWithConstructor()
{
	Euler e = Euler(0,0,0);
	char result = check(e.isEqual(Euler()), "shouldCreateWithConstructor");
	return result;
}

char EulerSpec::shouldConvertEulerToMatrix()
{
	Euler e = Euler(0, 0, 90);
	Matrix3 m = Matrix3::from(Euler(e.pitch, e.yaw, e.roll));
	Matrix3 result = Matrix3(0, -1, 0, 1, 0, 0, 0, 0, 1);
	char r = check(m.isEqual(result), "shouldConvertEulerToMatrix");
	return r;
}

char EulerSpec::shouldConvertEulerToAngleAxis() {
	Euler e = Euler(0, 0, 90);
	AxisAngle a = AxisAngle::from(e);
	char result = check(a.isEqual(AxisAngle(Vector3(1, 0, 0).asVersor(), a.angle)), "shouldConvertEulerToAngleAxis");
	return result;
}

char EulerSpec::shouldCheckEqualityCorrectly()
{
	Euler a = Euler(0, 0, 90);
	Euler b = Euler(0, 0, 90);

	char result = check(a == b, "shouldCheckEqualityCorrectly using == operator");
	result = result && check(a.isEqual(b), "shouldCheckEqualityCorrectly using isEqual");
	return result;
}

char EulerSpec::shouldApplyWithVector3Correctly()
{
	Euler a = Euler(0, 0, 0);
	Vector3 result = a.apply(Vector3(0, 0, 90));
	char r = check(result.isEqual(Vector3(0, 0, 90)), "shouldApplyWithVector3Correctly");
	return r;
}

char EulerSpec::shouldApplyXRotationCorrectly()
{
	Matrix3 a = Matrix3::rotationX(90);
	Euler e = Euler::from(a);
	char result = check(e.isEqual(Euler(90, 0, 0)), "shouldApplyXRotationCorrectly");
	return result;
}

char EulerSpec::shouldApplyYRotationCorrectly()
{
	Matrix3 a = Matrix3::rotationY(90);
	Euler e = Euler::from(a);
	char result = check(e.isEqual(Euler(0, 90, 0)), "shouldApplyYRotationCorrectly");
	return result;
}

char EulerSpec::shouldApplyZRotationCorrectly()
{
	Matrix3 a = Matrix3::rotationZ(90);
	Euler e = Euler::from(a);
	char result = check(e.isEqual(Euler(0, 0, 90)), "shouldApplyZRotationCorrectly");
	return result;
}

char EulerSpec::shouldConvertToMatrixAndBack()
{
	Matrix3 a = Matrix3::rotationX(90);
	Euler eFromM = Euler::from(a);
	Matrix3 mFromE = Matrix3::from(eFromM);
	char result = check(mFromE.isEqual(a), "shouldConvertToMatrixAndBack");
	return result;
}