#pragma once

#include "vector3.h"
#include "versor3.h"
/* Euler class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods */

/*
Ho scelto gli Euler angles come rotazione passando per le matrici di rotazione.
Questa scelta � meno performante rispetto all'utilizzo di quaternioni ma � comunque interessante.

La mia idea � in linea con quella di Unity, ovvero di usare angoli di Eulero, umanamente comprensibili, ma lavorando
con i quaternioni.

Per motivi di tempo non sono riuscito a completare i quaternioni, mantenendo cosi la logica sulle matrici, ma il mio obbiettivo � quello di completare
i quaternions e cambiare implementazione.
*/

class Quaternion;
class Point3;
class Matrix3;
class AxisAngle;

class Euler {
public:

	/* fields */
	// DONE E-Fields: which fields to store? (also add a constuctor taking these fields).

	Scalar pitch;   // x rot
	Scalar yaw;     // y rot
	Scalar roll;    // z rot

	// DONE E-Ide: this constructor construct the identity rotation
	Euler() :
		pitch(0), yaw(0), roll(0) {}
	Euler(Scalar _pitch, Scalar _yaw, Scalar _roll) :
		pitch(_pitch), yaw(_yaw), roll(_roll) {}
	// Done E-Constr
	// row major order!
	Euler(Scalar m00, Scalar m01, Scalar m02,
		Scalar m10, Scalar m11, Scalar m12,
		Scalar m20, Scalar m21, Scalar m22);
	bool operator ==(Euler b) const;
	Vector3 apply(Vector3 v) const;
	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const;
	Point3 apply(Point3 p) const;
	// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
	Versor3 operator() (Versor3 p);
	Point3  operator() (Point3  p);
	Vector3 operator() (Vector3 p);
	Versor3 axisX() const;
	Versor3 axisY() const;
	Versor3 axisZ() const;
	// conjugate
	Euler operator * (Euler b) const;
	Euler inverse() const;
	bool isEqual(const Euler& b);
	void invert();
	// specific methods for Eulers...
	Euler transposed() const;
	void transpose();
	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Euler lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());
	// returns a rotation
	static Euler toFrom(Versor3 to, Versor3 from);
	static Euler toFrom(Vector3 to, Vector3 from);
	// conversions to this representation
	static Euler from(Quaternion m);// TODO Q2M
	static Euler from(Euler e);
	static Euler from(AxisAngle e);
	static Euler from(Matrix3 m);
	// does this Euler encode a rotation?
	bool isRot() const;
	// return a rotation matrix around an axis
	static Euler rotationX(Scalar angleDeg);
	static Euler rotationY(Scalar angleDeg);
	static Euler rotationZ(Scalar angleDeg);
	void printf() const;
};


// interpolation or rotations
inline Euler directLerp(const Euler& a, const Euler& b, Scalar t);
inline Euler lerp(const Euler& a, const Euler& b, Scalar t);