#pragma once

#include "typedefs.h"

namespace Nica {
	template<class T>
	T clamp(T value, T min, T max) {
		return value;
	}
	template<>
	Scalar clamp(Scalar value, Scalar min, Scalar max);

	template<class T>
	T acos(T value) {
		return value;
	}

	template<>
	Scalar acos(Scalar value);


	template<class T>
	T cos(T value) {
		return value;
	}

	template<>
	Scalar cos(Scalar value);

	template<class T>
	T sin(T value) {
		return value;
	}

	template<>
	Scalar sin(Scalar value);

	template<class T>
	T asin(T value) {
		return value;
	}

	template<>
	Scalar asin(Scalar value);

	template<class T>
	T sqrt(T value) {
		return value;
	}

	template<>
	Scalar sqrt(Scalar value);

	template<class T, class U>
	T atan2(T a, U b) {
		return a;
	}
	
	template<>
	Scalar atan2(Scalar a, Scalar b);

	Scalar lerp(Scalar a, Scalar b, Scalar t);
	Scalar lerp(Scalar a, Scalar b, float t);

	Scalar fromRadToDegree(Scalar rad);
	Scalar fromDegreeToRad(Scalar degree);
	Scalar evaluateLimits(Scalar value, Scalar min = EPSILON, Scalar max = 1);
}