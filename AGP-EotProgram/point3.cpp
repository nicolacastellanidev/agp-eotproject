#include "point3.h"
#include "vector3.h"

Point3::Point3()
	:x(0), y(0), z(0) { } // origin

Point3::Point3(Scalar _x, Scalar _y, Scalar _z)
	:x(_x), y(_y), z(_z) { }

Point3 Point3::origin()
{
	return Point3(0, 0, 0);
}

Scalar& Point3::operator[](int i)
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}

Scalar Point3::operator[](int i) const
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}

Point3 Point3::operator-(const Vector3& b) const
{
	return Point3(x - b.x, y - b.y, z - b.z);
}

Point3 Point3::operator*(const Point3& b) const
{
	return Point3(x * b.x, y * b.y, z * b.z);
}

Vector3 Point3::operator-(const Point3& b) const
{
	return Vector3(x - b.x, y - b.y, z - b.z);
}

Point3 Point3::operator+(const Vector3& b) const
{
	return Point3(x + b.x, y + b.y, z + b.z);
}

Point3 Point3::operator+(const Point3& b) const
{
	return Point3(x + b.x, y + b.y, z + b.z);
}

Scalar Point3::getDistanceTo(const Point3& b) const
{
	// Get a point along the line
	Scalar x = (this->x - b.x);
	Scalar y = (this->y - b.y);
	Scalar z = (this->z - b.z);

	// calc the distance
	return Nica::sqrt(x * x + y * y + z * z);
}

void Point3::operator+=(const Vector3& b)
{
	x += b.x;
	y += b.y;
	z += b.z;
}

Point3 Point3::scaled(Scalar k) const
{
	return Point3(x * k, y * k, z * k);
}

Vector3 Point3::asVector() const
{
	//return *this - origin();

	return Vector3(x, y, z);
}

bool Point3::isEqual(const Point3& b)
{
	return squaredNorm(*this - b) < EPSILON2;
}

inline Point3 lerp(const Point3& a, const Point3& b, Scalar t)
{
	return a + t * (b - a);
}

inline Scalar squaredDist(const Point3& a, const Point3& b)
{
	return  squaredNorm(a - b);
}

inline Scalar dist(const Point3& a, const Point3& b)
{
	return norm(a - b);
}

