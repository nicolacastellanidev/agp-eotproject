#include "tests.h"
#include "euler.spec.h"
#include "problems.spec.h"
#include "quaternions.spec.h"
#include "Print.h"

#define MAX_ENPHASIS FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY | FOREGROUND_RED

int main()
{
	Nica::PrintTitle("Advanced Graphics Programming final exam");
	Nica::PrintSpaceBetween({ "[AUTHOR]" }, { "Nicola Castellani", "VR361872", "nicolacastellanidev@gmail.com" }, MAX_ENPHASIS);
	Nica::PrintNewLine();
	Nica::PrintSpaceBetween({ "[DESCRIPTION]" }, { "Playground for different rotations methods" }, MAX_ENPHASIS);
	Nica::PrintSpaceBetween({}, { "I've started with Euler angles, then Axis angles, Matrix and Quaternions" }, MAX_ENPHASIS);

	
	Nica::PrintHR();

	char res = 1;

	Nica::PrintTitle("Exam specs");

	res &= unitTestInverse();
	res &= unitTestCumulate();

	// EULER UNITS

	Nica::PrintTitle("Euler specs");

	res &= EulerSpec::shouldCreateWithDefaultConstructor();
	res &= EulerSpec::shouldCreateWithConstructor();
	res &= EulerSpec::shouldConvertEulerToMatrix();
	res &= EulerSpec::shouldConvertEulerToAngleAxis();
	res &= EulerSpec::shouldCheckEqualityCorrectly();
	res &= EulerSpec::shouldApplyWithVector3Correctly();
	res &= EulerSpec::shouldApplyXRotationCorrectly();
	res &= EulerSpec::shouldApplyYRotationCorrectly();
	res &= EulerSpec::shouldApplyZRotationCorrectly();
	res &= EulerSpec::shouldConvertToMatrixAndBack();

	// QUATERNIONS

	Nica::PrintTitle("Quaternions specs");

	res &= QuaternionSpecs::shouldCreateCorrectlyWithDefaultConstructor();
	res &= QuaternionSpecs::shouldCreateCorrectlyWithConstructorWithParams();
	res &= QuaternionSpecs::shouldCreateCorrectlyWithPoint3();
	res &= QuaternionSpecs::shouldReturnCorrectlyAxisX();
	res &= QuaternionSpecs::shouldReturnCorrectlyAxisY();
	res &= QuaternionSpecs::shouldReturnCorrectlyAxisZ();
	res &= QuaternionSpecs::shouldReturnCorrectlyIsRot();
	res &= QuaternionSpecs::shouldReturnCorrectlyIsPoint();

	// PROBLEMS

	Nica::PrintTitle("Problems specs");

	res &= ProblemsSpec::shouldIntersectRayOnPlane();
	res &= ProblemsSpec::shouldNotIntersectRayOnPlane();
	res &= ProblemsSpec::shouldEyeNotSeePointTooFar();
	res &= ProblemsSpec::shouldEyeSeePointNotTooFar();

	Nica::PrintTitle("Tests result", FOREGROUND_GREEN | FOREGROUND_RED);
	auto out = res ? "[SUCCESS]" : "[FAIL]";
	Nica::PrintSpaceBetween({ "Test result: " }, { out }, MAX_ENPHASIS);
	Nica::SetConsoleAttribute(); // force console reset

	return 0;
}
