#include "versor3.h"

Scalar Versor3::getAngleWith(const Versor3& b) const
{
	return Nica::acos(dot(*this, b));
}
