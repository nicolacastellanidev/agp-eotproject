#include <iostream>
#include "vector3.h"
#include "point3.h"
#include "Print.h"
#include "typedefs.h"

bool Vector3::operator==(const Vector3& other) const
{
	return squaredNorm(*this - other) <= EPSILON2;
}

void Vector3::operator+=(const Vector3& b)
{
	x += b.x;
	y += b.y;
	z += b.z;
}

Vector3::Vector3()
	:x(0), y(0), z(0) { }

Vector3::Vector3(Scalar _x, Scalar _y, Scalar _z)
	:x(_x), y(_y), z(_z) { }

Vector3::Vector3(const Point3& p)
	: x(p.x), y(p.y), z(p.z) { }

Vector3 Vector3::right()
{
	return Vector3(+1, 0, 0);
}

Vector3 Vector3::left()
{
	return Vector3(-1, 0, 0);
}

Vector3 Vector3::up()
{
	return Vector3(0, +1, 0);
}

Vector3 Vector3::down()
{
	return Vector3(0, -1, 0);
}

Vector3 Vector3::fowrard()
{
	return Vector3(0, 0, +1);
}
Vector3 Vector3::backward()
{
	return Vector3(0, 0, -1);
}

Vector3 Vector3::zero()
{
	return Vector3(0, 0, 0);
}

Scalar& Vector3::operator[](int i)
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	return x;
}

Scalar Vector3::operator[](int i) const
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}

Vector3 Vector3::operator*(const Vector3& b) const {
	return Vector3(b.x * x, b.y * y, b.z * z);
}

Vector3 Vector3::operator*(Scalar k) const {
	return Vector3(k * x, k * y, k * z);
}

Vector3 Vector3::operator/(Scalar k) const {
	return Vector3(x / k, y / k, z / k);
}

void Vector3::printf() const {
	std::cout
		<< Nica::PrintPair("x", x) << " "
		<< Nica::PrintPair("y", y) << " "
		<< Nica::PrintPair("z", z) << " "
		<< "\n";
} // Done

void Vector3::operator /= (Scalar k) {
	x /= k;
	y /= k;
	z /= k;
}

void Vector3::operator *= (Scalar k) {
	x *= k;
	y *= k;
	z *= k;
}

Vector3 Vector3::operator+(const Vector3& b) const
{
	return Vector3(x + b.x, y + b.y, z + b.z);
}

void Vector3::operator*=(const Vector3& b)
{
	x *= b.x;
	y *= b.y;
	z *= b.z;
}

bool Vector3::isEqual(const Vector3& b) const
{
	return x == b.x
		&& y == b.y
		&& z == b.z;
}

Vector3 Vector3::operator-(const Vector3& b) const
{
	return Vector3(x - b.x, y - b.y, z - b.z);
}

Vector3 Vector3::operator-() const
{
	return Vector3(-x, -y, -z);
}

Point3 Vector3::asPoint() const {
	return Point3(x, y, z);
}