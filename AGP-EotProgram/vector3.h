﻿#pragma once
#include "Math.h"

typedef double Scalar;

class Point3;
class Versor3;

class Vector3{
public:
    Scalar x,y,z;
    // constructors
    Vector3();
    Vector3(Scalar _x, Scalar _y, Scalar _z);
    Vector3(const Point3& p);
    static Vector3 right();
    static Vector3 left();
    static Vector3 up();
    static Vector3 down();
    static Vector3 fowrard();
    static Vector3 backward();
    static Vector3 zero();
    // access to the coordinates: to write them
    Scalar& operator[] (int i);
    // access to the coordinates: to read them
    Scalar operator[] (int i) const;
    // vector sum
    Vector3 operator+(const Vector3& b) const;
    Vector3 operator*(const Vector3& b) const;
    bool operator==(const Vector3& other) const;
    // vector-sum: in-place version
    void operator+=(const Vector3& b);
    Vector3 operator-(const Vector3& b) const;
    // unitary minus operator
    Vector3 operator-() const;
    Vector3 operator*(const Vector3& b);
    void operator *= (const Vector3& b);
    Vector3 operator*(Scalar k) const;
    void operator *= (Scalar k);
    Vector3 operator/(Scalar k) const;
    void operator /= (Scalar k);
    bool isEqual(const Vector3& b) const;
    Point3 asPoint() const;
    Versor3 asVersor() const;
    void printf() const; // Done
};


inline Scalar dot(const Vector3& a, const Vector3& b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Vector3 cross(const Vector3 &a, const Vector3 &b){
    return Vector3(
                a.y*b.z-a.z*b.y ,
                a.z*b.x-a.x*b.z ,
                a.x*b.y-a.y*b.x );
}

inline Scalar squaredNorm(const Vector3 &a){
    return dot(a,a);
}

inline Scalar squaredNorm(Vector3& a) {
	return dot(a, a);
}

inline Scalar norm(const Vector3& a) {
	return Nica::sqrt(squaredNorm(a));
}

inline Vector3 operator*( Scalar k, const Vector3& a){ return a*k; }

inline Vector3 lerp(const Vector3& a,const Vector3& b, Scalar t){
    return (1-t)*a + t*b;
}