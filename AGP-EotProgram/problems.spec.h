#pragma once

namespace ProblemsSpec {
	char shouldIntersectRayOnPlane();
	char shouldNotIntersectRayOnPlane();

	char shouldEyeSeePointNotTooFar();
	char shouldEyeNotSeePointTooFar();
}