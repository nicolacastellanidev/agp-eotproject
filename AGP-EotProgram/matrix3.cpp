#include "matrix3.h"
#include "point3.h"
#include "euler.h"
#include "vector3.h"

Matrix3::Matrix3()
{
	row1 = Versor3::zero();
	row2 = Versor3::zero();
	row3 = Versor3::zero();
}

Matrix3::Matrix3(Versor3 _r1, Versor3 _r2, Versor3 _r3)
	:row1(_r1), row2(_r2), row3(_r3) {}

Matrix3::Matrix3(Vector3 _r1, Vector3 _r2, Vector3 _r3)
	: row1(normalize(_r1)), row2(normalize(_r2)), row3(normalize(_r3)) {};

Matrix3::Matrix3(Scalar m00, Scalar m01, Scalar m02, Scalar m10, Scalar m11, Scalar m12, Scalar m20, Scalar m21, Scalar m22)
{

	row1 = normalize(Vector3(m00, m01, m02));
	row2 = normalize(Vector3(m10, m11, m12));
	row3 = normalize(Vector3(m20, m21, m22));
}

Vector3 Matrix3::apply(Vector3 v) const
{
	// TODO M-App: how to apply a rotation of this type?
	return Vector3();
}

Versor3 Matrix3::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 Matrix3::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

Versor3 Matrix3::operator()(Versor3 p)
{
	return apply(p);
}

Point3 Matrix3::operator()(Point3 p)
{
	return apply(p);
}

Vector3 Matrix3::operator()(Vector3 p)
{
	return apply(p);
}

bool Matrix3::isEqual(const Matrix3& b) const
{
	return row1 == b.row1
		&& row2 == b.row2
		&& row3 == b.row3;
}

Matrix3 Matrix3::operator*(Matrix3 r) const
{
	Matrix3 res;

	res.row1.x = row1.x * r.row1.x + row1.y * r.row2.x + row1.z * r.row3.x;
	res.row1.y = row1.x * r.row1.y + row1.y * r.row2.y + row1.z * r.row3.y;
	res.row1.z = row1.x * r.row1.z + row1.y * r.row2.z + row1.z * r.row3.z;

	res.row2.x = row2.x * r.row1.x + row2.y * r.row2.x + row2.z * r.row3.x;
	res.row2.y = row2.x * r.row1.y + row2.y * r.row2.y + row2.z * r.row3.y;
	res.row2.z = row2.x * r.row1.z + row2.y * r.row2.z + row2.z * r.row3.z;

	res.row3.x = row3.x * r.row1.x + row3.y * r.row2.x + row3.z * r.row3.x;
	res.row3.y = row3.x * r.row1.y + row3.y * r.row2.y + row3.z * r.row3.y;
	res.row3.z = row3.x * r.row1.z + row3.y * r.row2.z + row3.z * r.row3.z;

	return res;
}

Matrix3 Matrix3::inverse() const
{
	// TODO M-Inv a
	return Matrix3();
}

Vector3 Matrix3::operator*(Vector3 b)
{
	Vector3 result = Vector3::zero();

	result.x = row1.x * b.x + row1.y * b.y + row1.z * b.z;
	result.y = row2.x * b.x + row2.y * b.y + row2.z * b.z;
	result.z = row3.x * b.x + row3.y * b.y + row3.z * b.z;

	return result;
}

Versor3 Matrix3::operator*(Versor3 b)
{
	return (*this * b.asVector()).asVersor();
}

void Matrix3::invert() const
{
	// TODO M-Inv b
}

Matrix3 Matrix3::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	// TODO M-LookAt
	return Matrix3();
}

Matrix3 Matrix3::toFrom(Versor3 to, Versor3 from)
{
	// TODO M-ToFrom
	return Matrix3();
}

Matrix3 Matrix3::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from));
}

Matrix3 Matrix3::from(Euler e)
{
	// DONE E-App: how to apply a rotation of this type?
	Matrix3 RotX = rotationX(e.pitch);
	Matrix3 RotY = rotationY(e.yaw);
	Matrix3 RotZ = rotationZ(e.roll);
	Matrix3 RotYX = (RotY * RotX);
	Matrix3 result = RotZ * RotYX;
	return result; // zyx rotation
}

Scalar Matrix3::Det() const
{
	return row1.x * row2.y * row3.z
		+ row1.y * row2.z * row3.x
		+ row1.z * row2.x * row3.y
		- row3.x * row2.y * row1.z
		- row3.y * row2.z * row1.x
		- row3.z * row2.x * row2.y;
}

bool Matrix3::isRot() const
{
	// DONE M-isR
	bool IsRot = false;
	IsRot = (Det() - 1.0 <= EPSILON);
	IsRot = IsRot || cross(row1, row2) == row3.asVector();
	IsRot = IsRot || cross(row1, row3) == row2.asVector();
	IsRot = IsRot || cross(row3, row2) == row1.asVector();

	return IsRot;
}

Matrix3 Matrix3::rotationX(Scalar angleDeg)
{
	Scalar cos = Nica::cos(angleDeg);
	Scalar sin = Nica::sin(angleDeg);

	return Matrix3(
		1, 0, 0,
		0, cos, -sin,
		0, sin, cos
	);
}

Matrix3 Matrix3::rotationY(Scalar angleDeg)
{
	Scalar cos = Nica::cos(angleDeg);
	Scalar sin = Nica::sin(angleDeg);

	return Matrix3(
		cos, 0, sin,
		0, 1, 0,
		-sin, 0, cos
	);
}

Matrix3 Matrix3::rotationZ(Scalar angleDeg)
{
	Scalar cos = Nica::cos(angleDeg);
	Scalar sin = Nica::sin(angleDeg);

	return Matrix3(
		cos, -sin, 0,
		sin, cos, 0,
		0, 0, 1
	);
}

inline Matrix3 directLerp(const Matrix3& a, const Matrix3& b, Scalar t)
{
	// TODO M-directLerp: how to interpolate Matrix3s
	return Matrix3();
}

inline Matrix3 lerp(const Matrix3& a, const Matrix3& b, Scalar t)
{
	// TODO M-smartLerp: how to interpolate Matrix3s
	return Matrix3();
}
