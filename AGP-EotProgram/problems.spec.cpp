#include "problems.spec.h"
#include "Print.h"
#include "shared.h"
#include "problems.h"
#include "versor3.h"

typedef Point3 Eye;

char ProblemsSpec::shouldIntersectRayOnPlane()
{
	Ray r = { Point3(0, 10, 0), Vector3(0, -1, 1) };
	Plane p = { Point3(0, 0 ,0), Vector3::up().asVersor(), Vector3(10, 0, 10) }; 
	Point3 pointOnPlane = intersect(r, p);
	char result = check(!pointOnPlane.isEqual(NO_INTERSECTION), "shouldIntersectRayOnPlane");
	return result;
}

char ProblemsSpec::shouldNotIntersectRayOnPlane()
{
	Ray r = { Point3(0, 10, 0), Vector3(0, -1, 1) };
	Plane p = { Point3(100, 100 ,100), Vector3::up().asVersor(), Vector3(10, 0, 10) };
	Point3 pointOnPlane = intersect(r, p);
	char result = check(pointOnPlane.isEqual(NO_INTERSECTION), "shouldNotIntersectRayOnPlane");
	return result;
}

char ProblemsSpec::shouldEyeSeePointNotTooFar()
{
	Eye eye = Eye(0, 10, 0);
	Point3 target = Point3(0, 0, 0);
	Versor3 direction = Vector3::down().asVersor();
	Scalar angleDeg = 60; // fov angle
	Scalar maxDistance = 10 + EPSILON;
	char result = check(isSeen(eye, target, direction, angleDeg, maxDistance), "shouldEyeSeePointNotTooFar");
	return result;
}

char ProblemsSpec::shouldEyeNotSeePointTooFar()
{
	Eye eye = Eye(0, 10, 0);
	Point3 target = Point3(0, 0, 0);
	Versor3 direction = Vector3::down().asVersor();
	Scalar angleDeg = 60; // fov angle
	Scalar maxDistance = 10 - EPSILON;
	char result = check(!isSeen(eye, target, direction, angleDeg, maxDistance), "shouldEyeNotSeePointTooFar");
	return result;
}
