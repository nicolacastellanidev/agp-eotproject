#include "Math.h"
#include <cmath>

template<>
Scalar Nica::clamp(Scalar value, Scalar min, Scalar max) {
	return value < min ? min : value > max ? max : value;
}

template<>
Scalar Nica::acos(Scalar value)
{
	if (value > PI * 2) {
		// not in radians
		value = Nica::fromDegreeToRad(value);
	}
	return evaluateLimits(std::acos(value));
}

template<>
Scalar Nica::cos(Scalar value)
{
	if (value > PI * 2) {
		// not in radians
		value = Nica::fromDegreeToRad(value);
	}
	return evaluateLimits(std::cos(value));
}

template<>
Scalar Nica::sin(Scalar value)
{
	if (value > PI * 2) {
		// not in radians
		value = Nica::fromDegreeToRad(value);
	}
	return evaluateLimits(std::sin(value));
}

template<>
Scalar Nica::asin(Scalar value)
{
	if (value > PI * 2) {
		// not in radians
		value = Nica::fromDegreeToRad(value);
	}
	return evaluateLimits(std::asin(value), -PI / 2, PI / 2);
}

template<>
Scalar Nica::atan2(Scalar a, Scalar b)
{
	if (a > PI * 2) {
		// not in radians
		a = Nica::fromDegreeToRad(a);
	}
	if (b > PI * 2) {
		// not in radians
		b = Nica::fromDegreeToRad(b);
	}
	return evaluateLimits(std::atan2(a, b), -PI / 2, PI / 2);
}

Scalar Nica::lerp(Scalar a, Scalar b, Scalar t)
{
	return a * t + b * (1 - t);
}

Scalar Nica::lerp(Scalar a, Scalar b, float t)
{
	return a * t + b * (1 - t);
}

template<>
Scalar Nica::sqrt(Scalar value)
{
	return std::sqrt(value);
}


Scalar Nica::fromRadToDegree(Scalar rad)
{
	Scalar angle = RAD2DEG(rad);
	angle = fmod(angle, 360.0);
	if (angle < 0)
		angle += 360.0;
	return angle;
}

Scalar Nica::fromDegreeToRad(Scalar degree)
{
	if (degree < 0) degree += 360;
	if (degree > 360) degree -= 360;
	return degree * (PI / 180);
}

Scalar Nica::evaluateLimits(Scalar value, Scalar min, Scalar max)
{
	if (value <= min) return 0;
	if (value >= max) return 1;
	return value;
}
