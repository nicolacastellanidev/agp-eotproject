#include "versor3.h"
#include "vector3.h"
#include "Math.h"
#include "Print.h"
#include <iostream>
#include <cassert>

Scalar& Versor3::operator[](int i)
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}

Scalar Versor3::operator[](int i) const
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}
Vector3 Versor3::operator*(Scalar k) const
{
	return Vector3(k * x, k * y, k * z);
}

Versor3 Versor3::operator-() const
{
	return Versor3(-x, -y, -z);
}

Versor3 Versor3::operator-(Versor3 b) const
{
	return Versor3(x - b.x, y - b.y, z - b.z);
}

Versor3 Versor3::operator+() const
{
	return Versor3(x, y, z);
}

bool Versor3::operator==(Versor3 b) const
{
	return x == b.x && y == b.y && z == b.z;
}

Versor3 Versor3::asVersor() const
{
	// DONE: a nice assert?
	assert(squaredNorm(*this) - 1.0 <= EPSILON2);
	return Versor3(x, y, z);
}

Vector3 Versor3::asVector() const
{
	//return *this * 1;
	return Vector3(x, y, z);
}

void Versor3::printf() const
{
	std::cout
		<< Nica::PrintPair("x", x) << " "
		<< Nica::PrintPair("y", y) << " "
		<< Nica::PrintPair("z", z) << " "
		<< "\n";
} // Done

inline Versor3 normalize(Vector3 p)
{
	Scalar n = norm(p);
	if (n <= (0 + EPSILON)) {
		return p.asVersor();
	}
	return Versor3(p.x / n, p.y / n, p.z / n);
}

inline Scalar dot(const Versor3& a, const Versor3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Scalar dot(const Versor3& a, const Vector3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Vector3 cross(const Versor3& a, const Versor3& b)
{
	return Vector3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline Vector3 operator*(Scalar k, const Versor3& a)
{
	return a * k;
}

inline Versor3 nlerp(const Versor3& a, const Versor3& b, Scalar t)
{
	// DONE nlerp
	return normalize((1 - t) * a + t * b);
}

inline Versor3 slerp(const Versor3& a, const Versor3& b, Scalar t)
{
	// DONE slerp

	Scalar dotProduct = dot(a, b);
	// Clamp to prevent float precision issues
	Nica::clamp(dotProduct, -1.0, 1.0);
	// angle between versor
	Scalar alpha = Nica::acos(dotProduct);

	Vector3 d0_product = ((Nica::sin((1 - t) * alpha)) / Nica::sin(alpha)) * a;
	Vector3 d1_product = ((Nica::sin(t * alpha)) / Nica::sin(alpha)) * b;

	return normalize(d0_product + d1_product);
}

inline Scalar squaredNorm(const Versor3& a)
{
	return dot(a, a);
}
