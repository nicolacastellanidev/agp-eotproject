#pragma once

typedef double Scalar;

const Scalar EPSILON = 1e-6;
const Scalar EPSILON2 = EPSILON * EPSILON;
const Scalar PI = 3.141592653589793238463;
const Scalar PI_2 = PI*2;

#define RAD2DEG(rad) (rad * (180/PI))