#include "axis_angle.h"
#include "euler.h"
#include "Math.h"
#include "vector3.h"

AxisAngle::AxisAngle()
	: axis(Versor3::zero()), angle(0) {}

AxisAngle::AxisAngle(const Versor3& _axis, Scalar _angle)
	: axis(_axis), angle(_angle) {}

AxisAngle::AxisAngle(const Point3& p)
	: axis(p.asVector().asVersor()), angle(0) {}

Versor3 AxisAngle::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 AxisAngle::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

Versor3 AxisAngle::operator()(Versor3 p)
{
	return apply(p);
}

Point3 AxisAngle::operator()(Point3 p)
{
	return apply(p);
}

Vector3 AxisAngle::operator()(Vector3 p)
{
	return apply(p);
}

bool AxisAngle::isEqual(const AxisAngle& b)
{
	return squaredNorm((axis - b.axis).asVector()) < EPSILON2 && angle == b.angle;
}

AxisAngle AxisAngle::operator*(AxisAngle r) const
{
	return AxisAngle();
}

AxisAngle AxisAngle::inverse() const
{
	// TODO A-Inv a
	return AxisAngle();
}

void AxisAngle::invert() const
{
	// TODO A-Inv b
}

AxisAngle AxisAngle::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	// TODO A-LookAt
	return AxisAngle();
}

Vector3 AxisAngle::apply(Vector3 v) const 
{
	// TODO A-App: how to apply a rotation of this type?
	return Vector3();
}

AxisAngle AxisAngle::toFrom(Versor3 to, Versor3 from)
{
	Vector3 fromToOrtho = cross(from, to);
	Versor3 rotAxis = normalize(fromToOrtho);
	Scalar cosine = dot(from, to);
	Scalar angleRad = Nica::atan2(norm(fromToOrtho), cosine);

	return AxisAngle(rotAxis, angleRad);
}

AxisAngle AxisAngle::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from));
}

AxisAngle AxisAngle::from(Euler e)
{
	// TODO E2A
	// https://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToAngle/
	Scalar c1 = Nica::cos(e.yaw / 2);
	Scalar c2 = Nica::cos(e.pitch / 2);
	Scalar c3 = Nica::cos(e.roll / 2);
	Scalar s1 = Nica::sin(e.yaw / 2);
	Scalar s2 = Nica::sin(e.pitch / 2);
	Scalar s3 = Nica::sin(e.roll / 2);

	Scalar c1c2 = c1 * c2;
	Scalar s1s2 = s1 * s2;

	AxisAngle result;
	Scalar	w = c1c2 * c3 - s1s2 * s3;
	result.axis.x = c1c2 * s3 + s1s2 * c3;
	result.axis.y = s1 * c2 * c3 + c1 * s2 * s3;
	result.axis.z = c1 * s2 * c3 - s1 * c2 * s3;
	result.angle = Nica::fromRadToDegree(2 * Nica::acos(w));
	Scalar norm = result.axis.x * result.axis.x + result.axis.y * result.axis.y + result.axis.z * result.axis.z;

	if (norm < EPSILON) {
		result.axis.x = 1;
		result.axis.y = result.axis.z = 0;
	}
	else {
		norm = Nica::sqrt(norm);
		result.axis.x /= norm;
		result.axis.y /= norm;
		result.axis.z /= norm;
	}

	return result;
}

bool AxisAngle::isPoint() const
{
	// TODO A-isP
	return false;
}

inline AxisAngle lerp(const AxisAngle& a, const AxisAngle& b, Scalar t)
{
	// TODO A-Lerp: how to interpolate AxisAngles
	// hints: shortest path! Also, consider them are 4D unit vectors.
	return AxisAngle();
}